Document OWL 2 ontologies using off-the-shelf tools

- [Widoco](https://github.com/dgarijo/Widoco)

### Use ###

API entry points:

    documenter/*.php
	
parameters:

	owl2: {owl 2 specification as a string}
	documenter: {tool as a string. Ex. 'Widoco'}

### Contribution guidelines ###

* Clone, collaborate and enjoy

### Who do I talk to? ###

* german.braun@fi.uncoma.edu.ar