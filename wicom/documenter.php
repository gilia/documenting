<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   translator.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom;

use function \load;

use function \json_decode;

load("runner.php", "reasoner/");

use Wicom\Reasoner\Runner;


class Documenter{
    protected $tool = null;

    function __construct($tool){
        $this->tool = $tool;
    }

    function get_tool(){
        return $this->tool;
    }

    /**
       @param owl A String.
     */
    function to_docs($owl){
      $runner = new Runner($this->tool);
      $runner->run($owl);
      return $runner->get_last_answer();
    }

}
