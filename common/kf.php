<?php
/*

   Copyright 2018

   Author: GILIA

   uml.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom;

load("wicom.php");
load("documenter.php", "../wicom/");

load("runner.php", "../wicom/reasoner/");
load("widococonnector.php", "../wicom/reasoner/");


use Wicom\Documenter;

use Wicom\Reasoner\Runner;
use Wicom\Reasoner\WidocoConnector;

class KF_Wicom_Docs extends Wicom{

    function __construct(){
	     parent::__construct();
    }

    function document($owl_str, $tool = 'Widoco'){
      switch($tool){
          case 'Widoco' :
            $toolconnector = new WidocoConnector();
          break;
          default: throw new \Exception(
              "Tool $tool not Found!");
      }

      $doc = new Documenter($toolconnector);
      $id = $doc->to_docs($owl_str);

      return $id;
    }
}

?>
