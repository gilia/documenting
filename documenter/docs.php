<?php
/*

   Copyright 2016 GILIA

   Author: GILIA

   docs.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
   Documenting ontology.

   to be published

   Try this command:

   @return A Web page.
 */

require_once '../../common/import_functions.php';
load("config.php", "../../config/");
load('documenter.php', '../../wicom/');

load('kf.php','../../common/');
load('widococonnector.php','../../wicom/reasoner/');


use Wicom\Documenter;
use Wicom\KF_Wicom_Docs;
use Wicom\Reasoner\WidocoConnector;

$documenter = 'Widoco';
if (array_key_exists('documenter',$_REQUEST)){
    $documenter = $_REQUEST['documenter'];
}

if ( ! array_key_exists('owl2', $_POST)){
    echo " There's no \"owl2\" parameter :-(
      Use, for example:
    curl -d 'json={\"classes\": [{\"attrs\":[], \"methods\":[], \"name\": \"Hi World\"}]}' http://host.com/translator/crowd.php";
}else{

    switch ($documenter){
    case "Widoco" :
        $doc = new KF_Wicom_Docs();
        echo $doc->document($_POST['owl2'], 'Widoco');
        break;
    default:
        die("Invalid Format!");
    }

}

?>
