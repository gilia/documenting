<?php
/*

   Copyright 2017 GILIA

   Author: GILIA

   widocotest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

// use function \load;
load("config.php", "config/");
load("widococonnector.php", "wicom/reasoner/");

use Wicom\Reasoner\WidocoConnector;

class WidocoConnectorTest extends PHPUnit\Framework\TestCase
{

    public function testWidocoConnector(){
      $owl_input = file_get_contents("reasoner/data/testKFtoOWLAllQueries.owl");
      //$expected = file_get_contents("translator/strategies/data/testKFtoOWLAllQueries.owl");

      $trans = new WidocoConnector();
      $trans->run($owl_input);
    //var_dump($trans->get_col_answers()[0]);
      $this->assertNotNull($trans->get_col_answers()[0][0], "tests failed");
    }
}
