<?php
/*

   Copyright 2017 GILIA

   Author: GILIA

   widocotest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

// use function \load;
load("config.php", "config/");
load("kf.php", 'common/');
load("widococonnector.php", "wicom/reasoner/");

use Wicom\Reasoner\WidocoConnector;
use Wicom\KF_Wicom_Docs;

class DocumenterTest extends PHPUnit\Framework\TestCase
{

    public function testDocumenter(){
      $owl_input = file_get_contents("reasoner/data/testKFtoOWLAllQueries.owl");

      $trans = new KF_Wicom_Docs();
      $id = $trans->document($owl_input, 'Widoco');
      $this->assertNotNull($id, "tests failed");
    }
}
